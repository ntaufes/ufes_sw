#!/usr/bin/env python2
# -*- encoding: UTF-8 -*-

import os
import sys
import time
import rospy

import tf_bag

import rosbag
from sensor_msgs.msg import LaserScan

import numpy as np
import matplotlib.pyplot as plt

import matplotlib.patches as patches

from algs import *

def cartesian_product(*arrays):
    la = len(arrays)
    dtype = np.result_type(*arrays)
    arr = np.empty([len(a) for a in arrays] + [la], dtype=dtype)
    for i, a in enumerate(np.ix_(*arrays)):
        arr[...,i] = a
    return arr.reshape(-1, la)

mode = 'col'#'paradox'

def main():
	
	fig = plt.figure(figsize = (6.,8.))
	ax  = plt.axes()

	a1 = np.arange(-1.0, 3.0, 0.125)
	a2 = np.arange(-1.5, 1.5, 0.125)
	points = cartesian_product(a1,a2)
	v = np.array([-0.5, 0])


	
	if mode == 'pot' :
		vecs = map(lambda (x) : ( pot_field_4points(
		                                            [x],
		                                            dist=0.1,
		                                            max_force=25.0,
		                                            inner_radius=0.5,
		                                            outer_radius=1.5,
		                                           )[0]
		                          if (np.linalg.norm(x) >= 0.7)
		                          else [0.,0.]
		                        ),
		           points.tolist()
		          )
	elif mode == 'col' :
		vecs = map( lambda (x) : ( avoid_collision_point(
		                                               x, v,
		                                               F_max=25.0,
		                                               R_a=0.5,
		                                               R_o=0.2,
		                                               t_max=5.0
		                                            )[0]
		                          if (np.linalg.norm(x) >= 0.7)
		                          else [0.,0.]
		                        ),
		             points.tolist())
	elif mode == 'clo' :
		vecs = map( lambda (x) : ( avoid_closest_point(
		                                             x, v,
		                                             F_max=25.0,
		                                             R_a=0.5,
		                                             R_o=0.2,
		                                             R_d=0.3,
		                                             t_max=5.0
		                                            )[0]
		                          if (np.linalg.norm(x) >= 0.7)
		                          else [0.,0.]
		                        ),
		            points.tolist())
	elif mode == 'cri' :
		vecs = map( lambda (x) : ( avoid_critical_point(
		                                              x, v,
		                                              F_max=25.0,
		                                              R_a=0.5,
		                                              R_o=0.2,
		                                              R_d=0.3,
		                                              t_max=5.0
		                                             )[0]
		                          if (np.linalg.norm(x) >= 0.7)
		                          else [0.,0.]
		                        ),
		            points.tolist())
	elif mode == 'paradox' :
		points = np.array( [ [0.75 , -0.75] ,
		                     [0.75 ,  0.75] ]
		                 )
		vel1 = np.array( [-.15,0])
		vel2 = 0.15 * np.array([-3.,1.]) / np.sqrt(10.0)
		v1 = avoid_closest_point( points[0,:],
		                          vel1,
		                          F_max=1000.0,
		                          R_a=0.5,
		                          R_o=0.2,
		                          R_d=0.3,
		                          t_max=5.0
		                        )[0]
		v2 = avoid_closest_point( points[1,:],
		                          vel2,
		                          F_max=1000.0,
		                          R_a=0.5,
		                          R_o=0.2,
		                          R_d=0.3,
		                          t_max=5.0
		                        )[0]
		vecs = [v1,v2]
		qv = ax.quiver(-points[:,1],
		               points[:,0],
		               [-vel1[1],-vel2[1] ],
		               [ vel1[0], vel2[0] ],
		               color = 'g'
		              )

	ax.add_patch(
	             patches.Circle( (0.0, 0.0) , 0.7, color='k', fill=False)
	            )
	ax.add_patch(
	             patches.Circle( (0.0, 0.0) , 1.0, color='b', fill=False)
	            )
	ax.add_patch(
	             patches.Circle( (0.0, 0.0) , 1.5, color='c', fill=False)
	            )

	vecs = np.array(vecs)
	scale = np.linalg.norm(vecs,axis=1)
	scalelog = np.log(scale+1)
	
	q = ax.quiver(-points[:,1],
	              points[:,0],
	              -vecs[:,1] * scalelog / (scale + 0.01),
	              vecs[:,0] * scalelog / (scale + 0.01),
	              color = 'r',
	              scale = 75.0,
	             )


	plt.gca().set_xlim((-1.5,1.5))
	plt.gca().set_ylim((-1.,3.))
	plt.gca().set_autoscale_on(False) # Configuracao removida toda vez que clf ou cla eh chamado
	plt.gca().set_aspect('equal', 'box')
	plt.grid()
	fig.show()

	raw_input()
	return

	
	
	bag = rosbag.Bag(bagname)
	bag_transformer = tf_bag.BagTfTransformer(bag)
	trans = []
	quart = []
	for i in range(N) :
		translation, quaternion = bag_transformer.lookupTransform("world",
		                                                          "base_link_abs",
		                                                          rospy.Time(INIT_TIME + DT*i)
		                                                         )
		trans.append(translation)
		quart.append(quaternion)
	
	a = np.array(trans)
	a[:,2] = ( np.arange(INIT_TIME,LAST_TIME,DT) - INIT_TIME)[None,:]
	w1 = np.array( [ [-5.,1.5] , [20.,1.5] ] )
	w2 = np.array( [ [-5.,-1.5] , [20.,-1.5] ] )
	a_obst = np.concatenate( [ 10.0-0.3*a[:,2:3] , -0.5*np.ones(a.shape) ], axis=1)


	fig = plt.figure(figsize = (8.,8.))
	ax  = plt.axes()
	plot_traj_walker = plt.plot(-a[::5,1],a[::5,0],'ob', ms=4)
	plot_traj_walkerl = plt.plot(-a[:,1],a[:,0],'b')
	plot_traj_obst = plt.plot(-a_obst[:,1],a_obst[:,0],'r')
	plot_traj_walker = plt.plot(-a_obst[::5,1],a_obst[::5,0],'or', ms=4)
	plot_walls1 = plt.plot(-w1[:,1], w1[:,0], 'k')
	plot_walls2 = plt.plot(-w2[:,1], w2[:,0], 'k')

	dists = a[:,0:2]-a_obst[:,0:2]
	dists = np.linalg.norm(dists, axis = 1)
	idx = np.argmin(dists)
	
	for i in (0, N/3, idx):
		ax.add_patch(
		              patches.Circle( (-a[i,1], a[i,0]) , 0.5, color='k', fill=False)
		             )
		ax.add_patch(
		              patches.Circle( (-a_obst[i,1], a_obst[i,0]) , 0.2, color='r', fill=False)
		             )



	plt.gca().set_xlim((-2.,2.))
	plt.gca().set_ylim((-1.,10.))
	plt.gca().set_autoscale_on(False) # Configuracao removida toda vez que clf ou cla eh chamado
	plt.gca().set_aspect('equal', 'box')
	plt.grid()
	fig.show()
	raw_input()

	return trans,quart
	



if __name__ == "__main__" :
	main()
