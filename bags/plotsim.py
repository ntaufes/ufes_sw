#!/usr/bin/env python2
# -*- encoding: UTF-8 -*-

import os
import sys
import time
import rospy
import tf

import tf_bag

import rosbag
from sensor_msgs.msg import LaserScan

import numpy as np
import matplotlib.pyplot as plt

import matplotlib.patches as patches

from algs import *

INIT_TIME = 21.0
LAST_TIME = 38.0
DELTA_TIME = LAST_TIME - INIT_TIME
DT = 0.25
N = int(DELTA_TIME / DT)

def main(bagname):
	bag = rosbag.Bag(bagname)
	bag_transformer = tf_bag.BagTfTransformer(bag)
	trans = []
	quat = []
	for i in range(N) :
		translation, quaternion = bag_transformer.lookupTransform("world",
		                                                          "base_link_abs",
		                                                          rospy.Time(INIT_TIME + DT*i)
		                                                         )
		trans.append(translation)
		quat.append(tf.transformations.euler_from_quaternion(quaternion))

	a = np.array(trans)

	xforces = []
	current_time = INIT_TIME
	for topic, msg, t in bag.read_messages(topics = '/nav_xforce'):
		if rospy.Time(current_time) <= t:
			current_time = current_time + DT
			xforces.append(msg.data)
		if current_time >= LAST_TIME :
			break
	xforces = np.array(xforces)
	#print(xforces)
	yforces = []
	current_time = INIT_TIME
	for topic, msg, t in bag.read_messages(topics = '/nav_yforce'):
		if rospy.Time(current_time) <= t:
			current_time = current_time + DT
			yforces.append(msg.data)
		if current_time >= LAST_TIME :
			break
	yforces = np.array(yforces)
	#print(yforces)
	navf = np.concatenate( [xforces[:,None], yforces[:,None]] , axis=1)
	for i in range(N):
		navf[i] = np.matmul(navf[i],
		                    np.array([ [ np.cos(quat[i][2]) , np.sin(quat[i][2])] ,
		                               [-np.sin(quat[i][2]) , np.cos(quat[i][2])] ]
		                            )
		                   )


	a = np.array(trans)
	a[:,2] = ( np.arange(INIT_TIME,LAST_TIME,DT) - INIT_TIME)[None,:]
	a_obst = np.concatenate( [ 10.0-0.3*a[:,2:3] , -0.5*np.ones(a.shape) ], axis=1)


	fig = plt.figure(figsize = (8.,8.))
	ax  = plt.axes()
	plot_traj_walker = plt.plot(-a[::2,1],a[::2,0],'ob', ms=4)
	plot_vecs_navforce = ax.quiver(-a[::2,1], a[::2,0],
	                               -navf[::2,1], navf[::2,0],
	                               color = 'r',
	                               scale = 100.0
	                              )
	plot_traj_walkerl = plt.plot(-a[:,1],a[:,0],'b')
	plot_traj_obst = plt.plot(-a_obst[:,1],a_obst[:,0],'r')
	plot_traj_walker = plt.plot(-a_obst[::2,1],a_obst[::2,0],'or', ms=4)

	w1 = np.array( [ [-5.,1.5] , [20.,1.5] ] )
	w2 = np.array( [ [-5.,-1.5] , [20.,-1.5] ] )
	plot_walls1 = plt.plot(-w1[:,1], w1[:,0], 'k')
	plot_walls2 = plt.plot(-w2[:,1], w2[:,0], 'k')

	dists = a[:,0:2]-a_obst[:,0:2]
	dists = np.linalg.norm(dists, axis = 1)
	idx = np.argmin(dists)
	
	for i in (0, idx):
		ax.add_patch(
		              patches.Circle( (-a[i,1], a[i,0]) , 0.5, color='k', fill=False)
		             )
		ax.add_patch(
		              patches.Circle( (-a_obst[i,1], a_obst[i,0]) , 0.2, color='r', fill=False)
		             )



	plt.gca().set_xlim((-2.,2.))
	plt.gca().set_ylim((-1.,10.))
	plt.gca().set_autoscale_on(False) # Configuracao removida toda vez que clf ou cla eh chamado
	plt.gca().set_aspect('equal', 'box')
	plt.grid()
	fig.show()





	angforces = [ np.arctan2(x[1], x[0]) for x in navf ]
	absforces = np.linalg.norm(navf, axis=1)

	fig2, axs = plt.subplots(3, 1, sharex=True, figsize = (8.,6.))
	fig2.subplots_adjust(hspace=0)
	axs[0].plot(a[:,2], absforces, 'r')
	axs[0].set_ylim(0, 100)
	axs[1].plot(a[:,2], np.array(angforces)*180.0/np.pi, 'g')
	axs[1].set_yticks(np.array([-135, -90, -45, 0, 45, 90, 135]))
	axs[1].set_ylim(-180, 180)
	axs[2].plot(a[:,2], dists, 'b')
	axs[2].set_ylim(0, 10)
	axs[2].set_xlim(0,30)




	fig2.show()

	print("Menor distância: " + str(min(dists)) )
	print("Maior força: "+ str(max(absforces)) )
	print("Tempo colisão:" + str(a[idx,2]))

	raw_input()

	return trans,quat
	
	
	

	for msg in parsedlist :
		points = scan2array(msg)
		points = filter_dist_angle(points,
		                           msg.range_min, np.inf, # msg.range_max
		                           -np.pi/2, np.pi/2    )
		if points.shape[0] == 0 :
			continue
			#points = np.array( [ [np.inf, 0] ])
		x,y = pol2cart(points[:,0:1], points[:,1:2])
		y = y + LASER_OFFSET
		points = np.concatenate( (x,y), axis = 1)
		force,closest = pot_field_4points(points, outer_radius=4.0)
		plt.title('t=' + str(msg.header.stamp.to_sec()) + ' f=' + str(force))
		plot_points[0].set_data(-y,x)
		plot_nearest[0].set_data(-closest[:,1], closest[:,0])
		plot_force.U = -np.array((force[1],))
		plot_force.V = +np.array((force[0],))
		#
		# Versao ninja da internet:  # Versão lerda to tutorial:
		fig.canvas.draw()            # plt.draw()
		fig.canvas.flush_events()    # plt.pause(0.01)
		# fonte: http://bastibe.de/2013-05-30-speeding-up-matplotlib.html
		# Aparentemente, da pra otimizar ainda mais.
		#  A sacada e nao atualizar todo mundo, e sim só quem mudou. Chame
		#  ax.get_children() pra ver uma lista com todo mundo que é
		#  atualizado no flush_events
		#




if __name__ == "__main__" :
	main(sys.argv[1])
