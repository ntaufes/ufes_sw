#include <gazebo/gazebo.hh>

#include <thread>
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include "std_msgs/Float64.h"

namespace gazebo
{
	class HelloWorldPlugin : public WorldPlugin
	{
		public: HelloWorldPlugin() : WorldPlugin()
			{
			printf("Ola mundo\n");
			}
		public: void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
			{
			if (!ros::isInitialized())
				{
				int argc = 0;
				char **argv = NULL;
				ros::init(argc, argv, "gazebo_client",
				ros::init_options::NoSigintHandler);
				}
			this->rosNode.reset(new ros::NodeHandle("gazebo_client"));
			ros::SubscribeOptions so = ros::SubscribeOptions::create<std_msgs::Float64>
			  (
			  std::string("ola_mundo"),
			  1,
			  boost::bind(&HelloWorldPlugin::OnRosMsg, this, _1),
			  ros::VoidPtr(),
			  &this->rosQueue
			  );
			this->rosSub = this->rosNode->subscribe(so);
			this->rosQueueThread = std::thread(std::bind(&HelloWorldPlugin::QueueThread, this));
			}
		public: void OnRosMsg(const std_msgs::Float64ConstPtr &_msg)
			{
			ROS_INFO("veio\n");
			}
		private: void QueueThread()
			{
			static const double timeout = 0.01;
			while (this->rosNode->ok())
				{
				this->rosQueue.callAvailable(ros::WallDuration(timeout));
				}
			}
		private: std::unique_ptr<ros::NodeHandle> rosNode;
		private: ros::Subscriber rosSub;
		private: ros::CallbackQueue rosQueue;
		private: std::thread rosQueueThread;
		
	};
	GZ_REGISTER_WORLD_PLUGIN(HelloWorldPlugin)
}
