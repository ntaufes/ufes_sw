#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <boost/bind.hpp>

#include <string>

namespace gazebo
{
	class ConstantSpeedPlugin : public ModelPlugin
	{
		public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
			{
			this->model = _parent;
			this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&ConstantSpeedPlugin::OnUpdate, this, _1));
			std::string x("x"), y("y"), z("z");
			this->x = std::stod(_sdf->GetElement(x)->GetValue()->GetAsString().c_str());
			this->y = std::stod(_sdf->GetElement(y)->GetValue()->GetAsString().c_str());
			this->z = std::stod(_sdf->GetElement(z)->GetValue()->GetAsString().c_str());
			printf("x=%f y=%f z=%f\n", this->x, this->y, this->z);
			}
		public: void OnUpdate(const common::UpdateInfo &)
			{
			this->model->SetLinearVel(math::Vector3(this->x,this->y, this->z));
			}
		private: physics::ModelPtr model;
		private: event::ConnectionPtr updateConnection;
		private: double x;
		private: double y;
		private: double z;
	};
	GZ_REGISTER_MODEL_PLUGIN(ConstantSpeedPlugin)
}
