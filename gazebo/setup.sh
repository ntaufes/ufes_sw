#!/usr/bin/env bash

source /usr/share/gazebo/setup.sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

GAZEBO_MODEL_PATH=${DIR}/models:${GAZEBO_MODEL_PATH}
GAZEBO_PLUGIN_PATH=${DIR}/plugins/build:${GAZEBO_PLUGIN_PATH}
GAZEBO_RESOURCE_PATH=${DIR}/worlds:${GAZEBO_RESOURCE_PATH}

export GAZEBO_MODEL_PATH
export GAZEBO_PLUGIN_PATH
export GAZEBO_RESOURCE_PATH
