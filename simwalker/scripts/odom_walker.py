#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import rospy
import tf

from simwalker.msg import OdomWheels
from geometry_msgs.msg import TwistStamped

import numpy as np

x = 0.0
y = 0.0
th = 0.0

def callback(odom):
	global last_time
	global x, y, th
	timestamp = odom.header.stamp
	time = timestamp.to_sec()
	v = (odom.rwheel+odom.lwheel)*(wheel_radius)/2
	w = (odom.rwheel-odom.lwheel)*(wheel_radius)/(dist_wheels)
	try :
		dt = time - last_time
	except NameError :
		dt = 0.0
	# Publishing speeds:
	twist_msg = TwistStamped()
	twist_msg.header.stamp = timestamp
	twist_msg.twist.linear.x = v
	twist_msg.twist.angular.z = w
	pub_twist.publish(twist_msg)
	# Calculating displacement:
	D = v*dt
	dth = w*dt
	if dth != 0.0 :
		R = D/dth
		dx = R*np.sin(dth)
		dy = R * (1-np.cos(dth))
	else :
		dx = D
		dy = 0.0
	c = np.cos(th)
	s = np.sin(th)
	T_current_to_origin = np.array( [ [ c , -s ],
	                                  [ s ,  c ] ] )
	dr = np.array([dx, dy])
	dr = np.matmul(T_current_to_origin, dr)
	# Updating permanent data:
	x = x + dr[0]
	y = y + dr[1]
	th = th + dth
	last_time = time
	pub_tf.sendTransform( (x,y,0.0) ,
	                      tf.transformations.quaternion_from_euler(0.0,0.0,th) ,
	                      timestamp ,
	                      tf_to ,
	                      tf_from
	                    )

if __name__ == "__main__" :
	global dist_wheels, wheel_radius, pub_twist
	global pub_tf, tf_from, tf_to
	try:
		rospy.init_node('odom_walker')
		dist_wheels = rospy.get_param('dist_wheels')
		wheel_radius = rospy.get_param('wheel_radius')
		tf_from = rospy.get_param('~tf_from', 'world')
		tf_to = rospy.get_param('~tf_to', 'base_link_odom')
		pub_twist = rospy.Publisher('walker_velocity', TwistStamped, queue_size=10)
		pub_tf = tf.TransformBroadcaster()
		rospy.Subscriber('wheels_odom', OdomWheels, callback)
		rospy.spin()
	except rospy.ROSInterruptException:
		pass
