#!/usr/bin/env python2

import rospy
import tf

from gazebo_msgs.srv import GetModelState

if __name__ == "__main__":
	try:
		rospy.init_node("pub_base_link")
		name = rospy.get_param('~name', 'walker')
		tf_from = rospy.get_param('~tf_from', 'world')
		tf_to = rospy.get_param('~tf_to', 'base_link_gazebo')
		rate = rospy.get_param('~rate', 100)
		rate = rospy.Rate(rate)
		tfpub = tf.TransformBroadcaster()
		rospy.wait_for_service('gazebo/get_model_state')
		service_proxy = rospy.ServiceProxy('gazebo/get_model_state', GetModelState)
		while not rospy.is_shutdown():
			resposta = service_proxy(model_name = name)
			if resposta.success:
				x = resposta.pose.position.x
				y = resposta.pose.position.y
				z = resposta.pose.position.z
				qx = resposta.pose.orientation.x
				qy = resposta.pose.orientation.y
				qz = resposta.pose.orientation.z
				qw = resposta.pose.orientation.w
				tfpub.sendTransform( (x, y, z) ,
				                     (qx, qy, qz, qw) ,
				                      rospy.Time.now(),
				                      tf_to, tf_from)
			rate.sleep()
	except (rospy.ROSInterruptException, rospy.ServiceException):
		pass
