#!/usr/bin/env python2

import rospy
from std_msgs.msg import Header, Float64
from geometry_msgs.msg import Wrench

import exceptions

# Defaults:
xforce = 0.0
yforce = 0.0
dist_turnforce = 1.0 

def convert_cb(force, side):
	global pub, xforce, yforce, dist_turnforce
	if side == 'x' :
		xforce = force.data
	elif side == 'y' :
		yforce = force.data
	wrench = Wrench()
	wrench.force.x = xforce
	wrench.torque.z = dist_turnforce*yforce
	pub.publish(wrench)

def main():
	global pub, dist_turnforce
	rospy.init_node("forcesxy2wrench")
	dist_turnforce = rospy.get_param('dist_turnforce', dist_turnforce)
	pub = rospy.Publisher('wrench_out', Wrench, queue_size = 10)
	rospy.Subscriber("fx_in", Float64, lambda force : convert_cb(force,'x'))
	rospy.Subscriber("fy_in", Float64, lambda force : convert_cb(force,'y'))
	rospy.spin()

if __name__ == "__main__":
	try:
		main()
	except rospy.ROSInterruptException :
		pass
