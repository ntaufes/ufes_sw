#!/usr/bin/env python2

import rospy
from std_msgs.msg import Header, Float64
from geometry_msgs.msg import Wrench

import exceptions

# Defaults:
rforce = 0.0
lforce = 0.0
dist_handles = 1.0 

def convert_cb(force, side):
	global pub, lforce, rforce, dist_handles
	if side == 'left' :
		lforce = force.data
	elif side == 'right' :
		rforce = force.data
	wrench = Wrench()
	wrench.force.x = lforce + rforce
	wrench.torque.z = dist_handles*(rforce - lforce)
	pub.publish(wrench)

def main():
	global pub, lforce, rforce, dist_handles
	rospy.init_node("forceslr2wrench")
	dist_handles = rospy.get_param('dist_handles', dist_handles)
	pub = rospy.Publisher('wrench_out', Wrench, queue_size = 10)
	rospy.Subscriber("left_in", Float64, lambda force : convert_cb(force,'left'))
	rospy.Subscriber("right_in", Float64, lambda force : convert_cb(force,'right'))
	rospy.spin()

if __name__ == "__main__":
	try:
		main()
	except rospy.ROSInterruptException :
		pass
