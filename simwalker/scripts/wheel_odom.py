#!/usr/bin/env python2

import rospy
from std_msgs.msg import Float64
from gazebo_msgs.srv import GetJointProperties
from simwalker.msg import OdomWheels

def pool(name,rjoint, ljoint, wheeldiam, wheeldist, rate):
	pub = rospy.Publisher('wheels_odom', OdomWheels, queue_size=10)
	publ = rospy.Publisher('lwheel_odom', Float64, queue_size=10)
	pubr = rospy.Publisher('rwheel_odom', Float64, queue_size=10)
	rate = rospy.Rate(rate)
	try:
		rospy.wait_for_service('gazebo/get_joint_properties')
		service_proxy = rospy.ServiceProxy('gazebo/get_joint_properties', GetJointProperties)
		rospy.loginfo('name=' + name)
		rospy.loginfo('rjoint=' + rjoint)
		rospy.loginfo('ljoint=' + ljoint)
		while not rospy.is_shutdown():
			odom_direita = service_proxy(joint_name = name + '::' + rjoint)
			odom_esquerda = service_proxy(joint_name = name + '::' + ljoint)
			if odom_direita.success and odom_esquerda.success:
				o = OdomWheels()
				o.header.stamp = rospy.Time.now()
				o.rwheel = odom_direita.rate[0]
				o.lwheel = odom_esquerda.rate[0]
				pub.publish(o)
				publ.publish(Float64(o.lwheel))
				pubr.publish(Float64(o.rwheel))
			rate.sleep()
	except rospy.ServiceException:
		pass

if __name__ == '__main__':
	try:
		rospy.init_node('wheel_odom')
		name = rospy.get_param('~name', 'walker')
		rjoint = rospy.get_param('~rjoint', 'rjoint')
		ljoint = rospy.get_param('~ljoint', 'ljoint')
		wheeldiam = rospy.get_param('~wheel_diam', 0.1)
		wheeldist = rospy.get_param('~wheel_dist', 0.8)
		rate = rospy.get_param('~rate', 10.0)
		pool(name,rjoint,ljoint,wheeldiam,wheeldist,rate)
	except rospy.ROSInterruptException:
		pass
