#!/usr/bin/env python2

import sys
from threading import Lock

import rospy
from gazebo_msgs.srv import ApplyJointEffort, ApplyJointEffortRequest
from std_msgs.msg import Float64
from simwalker.msg import TorqueWheels

mutex = Lock()
torques = {'left': 0.0 , 'right': 0.0}

def update_torque(torque, which):
	with mutex:
		torques[which] = torque.data

def pool_repeat():
	rospy.init_node('wheel_motor')
	name = rospy.get_param('~name', 'walker')
	rjoint = rospy.get_param('~rjoint', 'rjoint')
	ljoint = rospy.get_param('~ljoint', 'ljoint')
	rate = rospy.get_param('~rate', 100.0)
	
	lcb = lambda x : update_torque(x,'left')
	rcb = lambda x : update_torque(x,'right')
	rospy.Subscriber('lwheel_ctl', Float64,lcb)
	rospy.Subscriber('rwheel_ctl', Float64,rcb)
	pub = rospy.Publisher('motor_ctl',TorqueWheels,queue_size=10)

	looprate = rospy.Rate(rate)
	try:
		rospy.wait_for_service('gazebo/apply_joint_effort')
		gazebo_apply_effort = rospy.ServiceProxy('gazebo/apply_joint_effort', ApplyJointEffort)
		while not rospy.is_shutdown():
			rreq = ApplyJointEffortRequest()
			lreq = ApplyJointEffortRequest()
			rreq.joint_name = name + '::' + rjoint
			lreq.joint_name = name + '::' + ljoint
			with mutex:
				rreq.effort = torques['right']
				lreq.effort = torques['left']
			rreq.duration = rospy.Duration(1.0/rate)
			lreq.duration = rospy.Duration(1.0/rate)
			gazebo_apply_effort(rreq)
			gazebo_apply_effort(lreq)
			pub.publish(TorqueWheels(rmotor = rreq.effort, lmotor = lreq.effort))
			looprate.sleep()
		pass
	except rospy.ServiceException:
		pass

if __name__ == '__main__':
	try:
		pool_repeat()
	except rospy.ROSInterruptException:
		pass

