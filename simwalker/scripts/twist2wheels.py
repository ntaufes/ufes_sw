#!/usr/bin/env python2

import rospy
from std_msgs.msg import Header, Float64
from geometry_msgs.msg import TwistStamped

# Defaults:
wheel_radius = 0.1
dist_wheels = 1.0 

def convert_cb(twist):
	global publ, pubr, dist_wheels, wheel_radius
	forcel = Float64()
	forcer = Float64()
	v = twist.twist.linear.x
	w = twist.twist.angular.z
	forcel.data = (v - w*dist_wheels/2) / wheel_radius
	forcer.data = (v + w*dist_wheels/2) / wheel_radius
	publ.publish(forcel)
	pubr.publish(forcer)

def main():
	global publ, pubr, dist_wheels, wheel_radius
	rospy.init_node("twist2wheelw")
	dist_wheels = rospy.get_param('dist_wheels', dist_wheels)
	wheel_radius = rospy.get_param('wheel_radius', wheel_radius)
	publ = rospy.Publisher('left_out', Float64, queue_size = 10)
	pubr = rospy.Publisher('right_out', Float64, queue_size = 10)
	rospy.Subscriber("twist_in", TwistStamped, convert_cb)
	rospy.spin()

if __name__ == "__main__":
	try:
		main()
	except rospy.ROSInterruptException :
		pass
