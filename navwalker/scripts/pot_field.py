#!/usr/bin/env python2

import exceptions

import rospy
import tf
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float64, String, UInt8, Int32, ColorRGBA, Bool, Duration, Header
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import Pose, Vector3, Point, Quaternion

from navwalker_algs import algs, rviz_helper

import numpy as np

LASER_OFFSET = 0.4
DIST_NO_REPULSION = 3.0
DIST_MAX_REPULSION = 0.5
MAX_FORCE_POTFIELD = 25.0
SHOW_FULL_FIELD = False

def callback(laser):
	global pubx, puby, pubmarker
	polar = algs.scan2array(laser)
	polar = algs.filter_dist_angle(
	               polar, laser.range_min, np.inf,
	               -np.pi/2, np.pi/2
	               )
	x,y = algs.pol2cart(polar[:,0], polar[:,1])
	y += LASER_OFFSET
	x,y = (x[:,None], y[:,None])
	xy = np.concatenate((x,y), axis=1)
	(force, closest) = algs.pot_field_4points(
	                          xy, max_force=MAX_FORCE_POTFIELD,
	                          inner_radius=DIST_MAX_REPULSION,
	                          outer_radius=DIST_NO_REPULSION
	                          )
	forcex = float(force[0])
	forcey = float(force[1])
	pubx.publish(Float64(forcex))
	puby.publish(Float64(forcey))
	if SHOW_FULL_FIELD :
		xvals = np.linspace(-1.0,5.0,30)
		yvals = np.linspace(-4.0,4.0,40)
		xgrid,ygrid = np.meshgrid(xvals,yvals)
		xgrid = xgrid.reshape((-1,1))
		ygrid = ygrid.reshape((-1,1))
		grid = np.concatenate( (xgrid,ygrid), axis=1)
		zgrid = [ algs.pot_field_calc(
		                 xy-p, 
		                 max_force=MAX_FORCE_POTFIELD,
		                 inner_radius=DIST_MAX_REPULSION,
		                 outer_radius=DIST_NO_REPULSION
		                 ) / MAX_FORCE_POTFIELD
		          for p in grid
		        ]
		zgrid = np.array(zgrid)[:,None]
		grid = np.concatenate( (grid,zgrid), axis=1)
		rviz_helper.show_field(pubmarker, grid, "base_link",
		                       laser.header.stamp)

def main_loop() :
	global pubx, puby, pubmarker, LASER_OFFSET, DIST_NO_REPULSION
        global DIST_MAX_REPULSION, MAX_FORCE_POTFIELD, SHOW_FULL_FIELD
	rospy.init_node("pot_field")
	rospy.Subscriber("laser", LaserScan, callback)
	pubx = rospy.Publisher("force_x", Float64, queue_size = 10)
	puby = rospy.Publisher("force_y", Float64, queue_size = 10)
	pubmarker = rospy.Publisher("navmarkers", MarkerArray, queue_size = 10)
	LASER_OFFSET = rospy.get_param('laser_offset', LASER_OFFSET)
	DIST_NO_REPULSION = rospy.get_param('~dist_no_repulsion', DIST_NO_REPULSION)
	DIST_MAX_REPULSION = rospy.get_param('~dist_max_repulsion', DIST_MAX_REPULSION)
	MAX_FORCE_POTFIELD = rospy.get_param('~max_force_potfield', MAX_FORCE_POTFIELD)
	SHOW_FULL_FIELD = rospy.get_param('~show_full_field', SHOW_FULL_FIELD)
	rospy.spin()

if __name__ == "__main__" :
	try:
		main_loop()
	except rospy.ROSInterruptException :
		pass

