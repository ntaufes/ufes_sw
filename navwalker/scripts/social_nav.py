#!/usr/bin/env python2

import exceptions

import rospy
import tf
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float64, String, UInt8, Int32, ColorRGBA, Bool, Duration, Header
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import Pose, Vector3, Point, Quaternion

from navwalker_algs import algs, rviz_helper

import numpy as np

LASER_OFFSET = 0.4
DIST_NO_REPULSION = 1.0
DIST_MAX_REPULSION = 0.5
MAX_FORCE_POTFIELD = 25.0
DIST_SOCIAL_COLLISION = 0.9
TIME_SOCIAL_COLLISION = 10.0
MAX_FORCE_SOCIAL = 25.0
SHOW_FULL_FIELD = False
ALG_TYPE = 'avoid_collision_point' # avoid_closest_point or avoid_critical_point or pot_field

WALKER_FRAME = "/base_link_est"
WORLD_FRAME = "/world"

tracker = algs.DBObjectTracker()

def callback(scan):
	global pubx, puby, pubmarker
	global last_time # static
	xy = algs.scan2xy_corrected(scan, LASER_OFFSET)
	time = scan.header.stamp
	try:
		dt = time.to_sec() - last_time.to_sec()
	except NameError:
		dt = 0.0
		last_time = time
	old_time = last_time
	last_time = time
	tf_listener.waitForTransformFull(WALKER_FRAME, old_time,
	                                 WALKER_FRAME, time,
	                                 WORLD_FRAME, rospy.Duration(1.0))
	T_last_to_current = tf_listener.lookupTransformFull(
	                      WALKER_FRAME, old_time, WALKER_FRAME, time, WORLD_FRAME)
	th = tf.transformations.euler_from_quaternion(T_last_to_current[1])[2]
	dx = T_last_to_current[0][0]
	dy = T_last_to_current[0][1]
	c = np.cos(th)
	s = np.sin(th)
	T = np.array( [ [  c  , s  , -( dx*c+dy*s) ] ,
	                [ -s  , c  , -(-dx*s+dy*c) ] ,
	                [  0. , 0. ,        1.     ] ] )
	#rospy.logwarn(T)
	if dt > 0 :
		v = -np.array([ T[0,2], T[1,2] ]) / dt
	else: 
		v = np.array([0.0, 0.0])
	if ALG_TYPE == 'pot_field' :
		(static_force, closest) = algs.pot_field_4points(
		                             pointcloud = xy,
		                             max_force = MAX_FORCE_POTFIELD,
		                             inner_radius = DIST_MAX_REPULSION,
		                             outer_radius = DIST_NO_REPULSION
		                             )
	else:
		tracker.update(xy, time.to_sec(), T)
		static_points = tracker.get_static_points()
		(static_force, closest) = algs.pot_field_4points(
		                            pointcloud = static_points,
		                            max_force = MAX_FORCE_SOCIAL, #MAX_FORCE_POTFIELD,
		                            inner_radius = DIST_MAX_REPULSION,
		                            outer_radius = DIST_NO_REPULSION
		                            )
	social_force = np.array([0.0,0.0])
	molist = []
	moindex = 2
	for mo in tracker.get_moving_objs():
		#f = algs.avoid_moving(mo[1][0:2], mo[3][0:2]-v, DIST_SOCIAL_COLLISION, TIME_SOCIAL_COLLISION)
		if ALG_TYPE == 'avoid_collision_point':
			f = algs.avoid_collision_point(mo[1][0:2], mo[3][0:2]-v,
			                               MAX_FORCE_SOCIAL,
			                               DIST_SOCIAL_COLLISION/2,
			                               DIST_SOCIAL_COLLISION/2,
			                               TIME_SOCIAL_COLLISION
			                              )
		elif ALG_TYPE == 'avoid_closest_point' :
			f = algs.avoid_closest_point(mo[1][0:2], mo[3][0:2]-v,
			                             MAX_FORCE_SOCIAL,
			                             DIST_SOCIAL_COLLISION/2,
			                             DIST_SOCIAL_COLLISION/2,
			                             0.3,
			                             TIME_SOCIAL_COLLISION
			                            )
		elif ALG_TYPE == 'avoid_critical_point' :
			f = algs.avoid_critical_point(mo[1][0:2], mo[3][0:2]-v,
			                              MAX_FORCE_SOCIAL,
			                              DIST_SOCIAL_COLLISION/2,
			                              DIST_SOCIAL_COLLISION/2,
			                              0.3,
			                              TIME_SOCIAL_COLLISION
			                             )
		elif ALG_TYPE == 'pot_field' :
			f = (np.array([0.0,0.0]), None)
		f, dangerpoint = f
		danger = np.linalg.norm(f) / MAX_FORCE_SOCIAL
		#f = MAX_FORCE_SOCIAL*f
		social_force = social_force + f
		if danger > 0 :
			molist.append( (mo[1][0:2], dangerpoint, (moindex,moindex+1) ) )
		else : 
			molist.append( (mo[1][0:2], mo[1][0:2], (moindex,moindex+1) ) )
		moindex = moindex + 2
	forcex = static_force[0] + social_force[0]
	forcey = static_force[1] + social_force[1]
	pubx.publish(Float64(forcex))
	puby.publish(Float64(forcey))
	mod_static_force = np.sqrt(np.sum(static_force**2))
	mod_social_force = np.sqrt(np.sum(social_force**2))
	ang_static_force = np.arctan2(static_force[1],static_force[0])
	ang_social_force = np.arctan2(social_force[1],social_force[0])
	rviz_helper.show_force_vector(pubmarker, WALKER_FRAME, mod_static_force,
	                              ang_static_force, (1.,0.,0.,1.), mid=0)
	rviz_helper.show_force_vector(pubmarker, WALKER_FRAME, mod_social_force,
	                              ang_social_force, (0.,1.,0.,1.), mid=1)
	for m in molist:
		rviz_helper.show_obst(pubmarker, m[0], m[1], WALKER_FRAME, time,
		                      (1.,1.,1.,1.), mid = m[2])
	
	#rospy.loginfo(str((static_force, social_force)))
	#if SHOW_FULL_FIELD :
	#	xvals = np.linspace(-1.0,5.0,30)
	#	yvals = np.linspace(-4.0,4.0,40)
	#	xgrid,ygrid = np.meshgrid(xvals,yvals)
	#	xgrid = xgrid.reshape((-1,1))
	#	ygrid = ygrid.reshape((-1,1))
	#	grid = np.concatenate( (xgrid,ygrid), axis=1)
	#	zgrid = [ algs.pot_field_calc(
	#	                 xy-p, 
	#	                 max_force=MAX_FORCE_POTFIELD,
	#	                 inner_radius=DIST_MAX_REPULSION,
	#	                 outer_radius=DIST_NO_REPULSION
	#	                 ) / MAX_FORCE_POTFIELD
	#	          for p in grid
	#	        ]
	#	zgrid = np.array(zgrid)[:,None]
	#	grid = np.concatenate( (grid,zgrid), axis=1)
	#	rviz_helper.show_field(pubmarker, grid, "base_link",
	#	                       laser.header.stamp)

def main_loop() :
	global pubx, puby, pubmarker, tf_listener
	rospy.init_node("social_nav2")
	update_params()
	pubx = rospy.Publisher("force_x", Float64, queue_size = 10)
	puby = rospy.Publisher("force_y", Float64, queue_size = 10)
	pubmarker = rospy.Publisher("navmarkers", MarkerArray, queue_size = 10)
	tf_listener = tf.TransformListener()
	rospy.Subscriber("laser", LaserScan, callback)
	rospy.spin()

def update_params() :
	global LASER_OFFSET
	global DIST_NO_REPULSION, DIST_MAX_REPULSION, MAX_FORCE_POTFIELD
	global DIST_SOCIAL_COLLISION, TIME_SOCIAL_COLLISION, MAX_FORCE_SOCIAL
	global SHOW_FULL_FIELD
	global WALKER_FRAME, WORLD_FRAME
	global ALG_TYPE
	LASER_OFFSET = rospy.get_param('laser_offset', LASER_OFFSET)
	DIST_NO_REPULSION = rospy.get_param('~dist_no_repulsion', DIST_NO_REPULSION)
	DIST_MAX_REPULSION = rospy.get_param('~dist_max_repulsion', DIST_MAX_REPULSION)
	MAX_FORCE_POTFIELD = rospy.get_param('~max_force_potfield', MAX_FORCE_POTFIELD)
	DIST_SOCIAL_COLLISION = rospy.get_param('~dist_social_collision', DIST_SOCIAL_COLLISION)
	TIME_SOCIAL_COLLISION = rospy.get_param('~time_social_collision', TIME_SOCIAL_COLLISION)
	MAX_FORCE_SOCIAL = rospy.get_param('~max_force_social', MAX_FORCE_SOCIAL)
	SHOW_FULL_FIELD = rospy.get_param('~show_full_field', SHOW_FULL_FIELD)
	WALKER_FRAME = rospy.get_param('~walker_frame', WALKER_FRAME)
	WORLD_FRAME = rospy.get_param('~world_frame', WORLD_FRAME)
	ALG_TYPE = rospy.get_param('~alg_type', ALG_TYPE) ##############

if __name__ == "__main__" :
	try:
		main_loop()
	except rospy.ROSInterruptException :
		pass

