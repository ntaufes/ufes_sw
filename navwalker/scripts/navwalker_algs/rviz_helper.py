#!usr/bin/env python2
# -*- encoding: UTF-8 -*-

import tf
import rospy

from std_msgs.msg import ColorRGBA, Duration, Header
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import Pose, Vector3, Quaternion, Point


def make_basic_marker(frameid, ns='walker', mid=0, time=None):
	m = Marker()
	m.header = Header()
	if time == None :
		m.header.stamp = rospy.Time.now()
	else :
		m.header.stamp = time
	m.header.frame_id = frameid
	m.ns = ns
	m.id = mid
	m.type = Marker.ARROW ##
	m.action = Marker.ADD
	m.pose = Pose() ##
	m.scale = Vector3(x=1.0, y=1.0, z=1.0) ##
	m.color = ColorRGBA(r=1.0, g=0.5, b=0.7, a=1.0) ##
	m.lifetime = rospy.Duration(0.2)
	m.frame_locked = False
	m.points = [] ##
	m.colors = [] ##
	m.text = ""
	m.mesh_resource = "" ##
	m.mesh_use_embedded_materials = False
	return m

def show_force_vector(pubmarker, frameid, rho, angle, color=(1.,.0,.0,1.), ns='walker', mid=0):
	if rho == 0:
		rho = 0.000001
	m = make_basic_marker(frameid, ns, mid)
	m.frame_locked = True
	m.type = Marker.ARROW
	m.action = Marker.ADD
	orientation = tf.transformations.quaternion_from_euler(0.0,0.0,angle)
	m.pose.orientation = Quaternion(x=orientation[0], y=orientation[1], z=orientation[2], w=orientation[3])
	m.scale = Vector3(x=rho/10, y=min(0.04, rho/50), z=min(rho/50,0.04))
	m.color = ColorRGBA(*color)
	pubmarker.publish(MarkerArray([m]))

def show_field(pubmarker, vals, frameid, time, ns='walker', mid=0):
	m = make_basic_marker(frameid, ns, mid, time=time)
	m.type = Marker.POINTS
	m.scale = Vector3(x=0.02, y=0.02, z=0.02)
	m.color = ColorRGBA(r=0.3, g=1.0, b=0.3, a=1.0)
	m.points = []
	for p in vals :
		m.points.append(Point(x=p[0], y=p[1], z=p[2]))
	pubmarker.publish(MarkerArray([m]))

def show_obst(pubmarker, origin, collision, frameid, time, color, ns='walker', mid=(0,1)):
	mobst = make_basic_marker(frameid, ns, mid[0], time=time)
	mobst.type = Marker.SPHERE
	mobst.scale = Vector3(x=0.5, y=0.5, z=0.5)
	mobst.color = ColorRGBA(*color)
	mobst.pose.position.x = origin[0]
	mobst.pose.position.y = origin[1]

	collision_marker = make_basic_marker(frameid, ns, mid[1], time=time)
	collision_marker.type = Marker.SPHERE
	collision_marker.scale = Vector3(x=.1, y=.1, z=.1)
	collision_marker.color = ColorRGBA(*color)
	collision_marker.pose.position.x = collision[0]
	collision_marker.pose.position.y = collision[1]
	
	pubmarker.publish(MarkerArray([mobst, collision_marker]))
