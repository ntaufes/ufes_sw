#!/usr/bin/env python2
# -*- encoding: UTF-8 -*-

import numpy as np

from sklearn.cluster import DBSCAN

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

def filter_dist_angle(points, min_dist, max_dist, min_angle, max_angle):
	"""Recebe ndarray nx2 com o par (r, theta) e retorna ele filtrado."""
	current = 0
	for i in range(points.shape[0]) :
		(radius, angle) = points[i,0:2]
		if (radius>min_dist and radius<max_dist and
	            angle>min_angle and angle<max_angle) :
			points[current,:] = (radius,angle)
			current = current + 1
	points = points[0:current, :]
	return points

def scan2xy_corrected(scan, offset) :
	polar = scan2array(scan)
	polar = filter_dist_angle(polar, scan.range_min, scan.range_max,
	                          scan.angle_min, scan.angle_max)
	x,y = pol2cart(polar[:,0:1], polar[:,1:2])
	x = x + offset
	points = np.concatenate( (x,y), axis=1)
	return points

def find_closest(pointcloud):
	"""Recebe ndarray nx2 c m coordenadas x,y"""
	dists = np.sum(pointcloud * pointcloud, axis=1)
	if len(dists) > 0 :
		index = np.argmin(dists, axis=0)
	else:
		return np.array([ np.inf, np.inf])
	return pointcloud[index, 0:2]

def avoid_closest(pointcloud, max_force=30.0, inner_radius=0.3,
                  outer_radius=3.0, custom_func=None):
	rho,theta = cart2pol(closest[0], closest[1])
	if custom_func == None:
		if rho>inner_radius and rho<outer_radius:
			force = -max_force * \
			        (inner_radius**2)/(rho**2) * \
			        np.array([np.cos(theta), np.sin(theta)])
		elif rho<inner_radius:
			force = -max_force * \
			        np.array([np.cos(theta), np.sin(theta)])
		else :
			force = np.array((0,0))
	else:
		force = -np.array([np.cos(theta), np.sin(theta)])*custom_func(rho)
	return force, closest

def pot_field_calc(pointcloud, dist=0.1, max_force=30.0, inner_radius=0.3,
                   outer_radius=3.0, custom_func=None) :
	dx = np.array([[dist, 0.0]])
	dy = np.array([[0.0, dist]])
	p = find_closest(pointcloud+dist)
	p = cart2pol(p[0],p[1])[0]
	if custom_func == None :
		custom_func = lambda r : lin_decay_field(r,max_force,outer_radius,inner_radius)
	p = custom_func(p)
	return p 

def pot_field_4points(pointcloud, dist=0.1, max_force=30.0, inner_radius=0.3,
                      outer_radius=3.0, custom_func=None):
	dx = np.array([[dist, 0.0]])
	dy = np.array([[0.0, dist]])
	(px, nx, py, ny) = ( find_closest(pointcloud+delta) for delta in (-dx, dx, -dy, dy) )
	closest = np.concatenate( (px[None,:],nx[None,:],py[None,:],ny[None,:]), axis=0)
	(px, nx, py, ny) = ( cart2pol(p[0],p[1]) [0] for p in (px, nx, py, ny) )
	if custom_func == None :
		custom_func = lambda r : lin_decay_field(r,max_force,outer_radius,inner_radius)
	(px, nx, py, ny) = map(custom_func, (px, nx, py, ny))
	xforce = -(px-nx)/(2*dist)
	yforce = -(py-ny)/(2*dist)
	return np.array([xforce, yforce]), closest

def lin_decay_force(r, FMAX, RMAX, RMIN = 0.0) :
	if r <= RMIN : 
		return FMAX
	elif r >= RMAX :
		return 0.0
	elif (r>RMIN) and (r<RMAX) :
		return FMAX*(RMAX-r)/(RMAX-RMIN)

def lin_decay_field(r, FMAX, RMAX, RMIN = 0.0) :
	if r >= RMAX :
		return 0.0
	elif r <= RMIN :
		return FMAX*(RMIN-r) + (FMAX/2)*(RMAX-RMIN)
	elif (r>RMIN) and (r<RMAX) :
		return FMAX/2 * (RMAX - r)**2 / (RMAX-RMIN)
	
def square_decay(r, outer_radius, inner_radius = 0.0):
	if r >= outer_radius :
		return 0.0
	elif r < 0.0 :
		return 1.0
	else :
		return (1.0 - (r-inner_radius)/(outer_radius-inner_radius))**2

def scan2array(scan):
	min_ang = scan.angle_min
	max_ang = scan.angle_max
	inc_ang = scan.angle_increment
	angles = np.arange(min_ang, max_ang + inc_ang/2, inc_ang)[:,None]
	ranges = np.array(scan.ranges)[:,None]
	points = np.concatenate( (ranges,angles), axis=1)
	return points

class TrackedObject : # USA COORDENADAS HOMOGENEAS. SEJA CONSISTENTE!
	STATIC_TO_MOBILE_LIMIT = 1.5
	FILTER_FREQ = 1.0
	STATIC = 1
	MOBILE = 2
	def __init__(self, points, id) :
		self.id = id
		self.type = TrackedObject.STATIC
		self.points = points
		self.position = np.mean(points, axis=0)
		self.init_position = self.position
		self.speed = np.array( (0.0,0.0,0.0) ) # Terceira coordenada homogenea
		self._age = 0 # 0 is newly created
		self.last_time = 0.0
	def update(self, points, t):
		new_pos = np.mean(points, axis=0)
		if self.type == TrackedObject.STATIC :
			total_disp = np.sum((new_pos-self.init_position)**2)
			total_disp = np.sqrt(total_disp)
			if total_disp > TrackedObject.STATIC_TO_MOBILE_LIMIT :
				self.type = TrackedObject.MOBILE
		if self.type == TrackedObject.MOBILE :
			dt = t-self.last_time
			if dt > 0 :
				k = np.exp(-dt*TrackedObject.FILTER_FREQ)
				new_speed = (new_pos - self.position) / (t-self.last_time)
				self.speed = k*self.speed + (1.0-k)*new_speed
		self.position = new_pos
		self.points = points
		self.last_time = t
	def age(self) :
		self._age = self._age + 1
	def refresh(self) :
		self._age = 0
	def transform(self, T):
		self.points = np.matmul(T, self.points.T).T
		self.speed = np.matmul(T, self.speed)
		self.position = np.matmul(T,self.position)
		self.init_position = np.matmul(T,self.init_position)
	def make_tuple(self):
		return ( self.id,
		         self.position,
		         self.points, 
		         self.speed,
		         self.init_position,
		         self._age
		       )

class DBObjectTracker:
	def __init__(self, group_dist=0.2, min_samples=3, max_dist=0.4, age_limit=10) :
		self.obj_index = 0
		self.objects = []
		self.dead_objects = []
		self.alg = DBSCAN(eps=group_dist, min_samples=min_samples)
		self.max_dist = max_dist
		self.age_limit = age_limit
		self.outliers = np.zeros( (0,2) )

	def update(self, points, t,  T=np.eye(3)) :
		"""Atualiza status do tracker com as leituras em points. A
		variável points deve ser um ndarray de nx2 pontos, com as 
		coordenadas xy das leituras. t é o tempo atual (float), e é
		usado nos cálculos de velocidade. T deve ser um ndarray 3x3
		com a transformação homogenea a ser aplicada às leituras
		anteriores para atualiza-las à nova posição do andador:

		p_new_ref = T*p_old_ref
	
		    [  cos | sin | -( +dx*cos + dy*sin ) ]
		T = [ -sin | cos | -( -dx*sin + dy*cos ) ]
		    [   0  |  0  |            1          ]
		
		Walls and big objects are not tracked. They appear in the 
		points returned by get_static_points, however, and are
		stored in the instance variable outliers.
		"""
		self._odom_update(T)
		if len(points) == 0:
			outliers = np.zeros((0,2))
			newobjs = []
		else:
			db = self.alg.fit(points)
			labels = db.labels_
			outliers, newobjs = self._separate_by_label(points,labels)
		newobjs = self._update_objects(newobjs, t)
		# Remove walls
		self.outliers = outliers # remove walls uses self.outliers
		newobjs = self._wall_removal(newobjs)
		# Add new objects to tracked objects
	 	for no in newobjs :
			padding = np.ones((no.shape[0],1))
			no = np.concatenate( (no,padding), axis=1)
			self.objects.append(TrackedObject(no, self.obj_index))
			self.obj_index = self.obj_index + 1
		# Remove old objects:
		self._kill_old()

	def get_static_points(self):
		objs = self.get_static_objs()
		objs = map( lambda x : x[2][:,0:2] ,
		            objs )
		objs.append(self.outliers)
		objs.append(np.zeros((0,2)))
		return np.concatenate(objs, axis=0)
	
	def get_moving_objs(self):
		objs = [obj for obj in self.objects if obj.type == TrackedObject.MOBILE]
		rets = []
		for obj in objs:
			rets.append(obj.make_tuple())	
		return rets

	def get_static_objs(self):
		objs = [obj for obj in self.objects if obj.type == TrackedObject.STATIC]
		rets = []
		for obj in objs:
			rets.append(obj.make_tuple())
		return rets

	def _kill_old(self):
		self.dead_objects = ( self.dead_objects +
		                      [each for each in self.objects if each._age>=self.age_limit]
		                    )
		self.objects = [each for each in self.objects if each._age<self.age_limit]

	def _odom_update(self, T) :
		for obj in self.objects:
			obj.transform(T)

	def _separate_by_label(self, points, labels):
		"""Returns a list of objects (as pointclouds) and a ndarray of 
		outliers."""
		outliers = [np.zeros((0,2))]
		objects = dict()
		for i in range(len(labels)):
			l = labels[i]
			p = points[i,:][None,:]
			if l == -1 :
				outliers.append(p)
			elif l in objects :
				objects[l].append(p)
			else :
				objects[l] = [p]
		outliers = np.concatenate(outliers,axis=0)
		objects = map(np.concatenate, objects.values())
		return outliers, objects

	def _update_objects(self, new_objects, t):
		"""Updates the tracked objects with the pointclouds in new_objects and
		returns a list with the pointclouds that didn't match any tracked object.
		Also updates the age of all tracked points.
		"""
		mapping = { 'not_found' : [] } # Maps tracked objects to elements in new_objects.
		known_positions = [each.position[:,None] for each in self.objects]
		if len(known_positions) != 0 :
			known_positions = np.concatenate(known_positions, axis=1)[0:2,:]
			for i in range(len(new_objects)):
				points = new_objects[i]
				pos = np.mean(points, axis=0)[:,None]
				dists = pos - known_positions
				dists = np.sum(dists**2,axis=0)
				closest_index = np.argmin(dists)
				closest_dist = dists[closest_index]
				if closest_dist < self.max_dist :
					if closest_index in mapping:
						mapping[closest_index].append(i)
					else :
						mapping[closest_index] = [i]
				else :
					mapping['not_found'].append(i)
		else:
			for i in range(len(new_objects)) :
				mapping['not_found'].append(i)
		for index in range(len(self.objects)) :
			if index in mapping :
				found_object = self.objects[index]
				fitting_indexes = mapping[index]
				matching_clouds = [new_objects[each] for each in fitting_indexes]
				newpoints = np.concatenate(matching_clouds, axis=0)
				padding = np.ones((newpoints.shape[0],1))
				newpoints = np.concatenate((newpoints,padding), axis=1)
				found_object.update(newpoints, t)
			else :
				self.objects[index].age()
		return [new_objects[i] for i in mapping['not_found']]

	def _wall_removal(self,objects):
		remaining = []
		walls = []
		for o in objects:
			m = np.mean(o, axis=0)
			points = o - m
			covmat = np.matmul(points.T, points)
			w,v = np.linalg.eig(covmat)
			ratio = w[0] / w[1]
			if ratio < 1 :
				ratio = 1/ratio
			if ratio < 100 :  # uma dimensão com desvio padrao 10x maior
				remaining.append(o)
			else :
				walls.append(o[:,0:2])
		self.outliers = np.concatenate( [np.zeros((0,2))]+[self.outliers]+walls, axis=0)
		return remaining

def social_nav(tracker, v_xy, dist=0.1, max_force=30.0, inner_radius=0.3,
               outer_radius=3.0, custom_func=None):
	"""Returns social force and static object force"""
	static_points = tracker.get_static_points()
	static_force, _ = pot_field_4points(static_points, dist, max_force, inner_radius,
	                                    outer_radius, custom_func)
	mov_obj = tracker.get_moving_objs()
	soc_force = np.array([ 0. , 0. ])
	for mo in mov_obj :
		p0 = mo[1][0:2]
		v = mo[3][0:2] - v_xy
		mo_force = max_force*avoid_moving(p0,v)[0]
		soc_force = soc_force + mo_force
	return (static_force, soc_force)

def avoid_moving(p0, v, R=2.0, T=10.0):
	t_danger = -( R/(2*T) + np.dot(p0,v) ) / np.dot(v,v)
	if t_danger < 0 :
		t_danger = 0.0
	elif t_danger > T :
		t_danger = T
	xf = p0[0] + t_danger * v[0]
	yf = p0[1] + t_danger * v[1]
	DANGER = 1 - (xf**2 + yf**2)/R**2 - t_danger/T
	danger_point = np.array([xf,yf])
	if DANGER > 0 :
		force = -np.array([xf,yf])
		force = force / np.linalg.norm(force)
		force = DANGER * force
	else :
		force = np.array([0.,0.])
	return (force, DANGER, danger_point)

def avoid_collision_point(p0, v, F_max=25.0, R_a=0.6, R_o=0.3, t_max=5.0, func_t=None):
	force = np.array([0.,0.])
	p_col = None
	R_col = R_a + R_o
	if np.linalg.norm(v) == 0.0 :
		return ( force, p_col )
	r_min = p0 - v * np.dot(p0,v) / np.linalg.norm(v)**2
	r_min_norm = np.linalg.norm(r_min)
	if r_min_norm >= R_col :
		return ( force, p_col )
	x0,y0 = (p0[0], p0[1])
	xv,yv = ( v[0], v[1] )
	# bhaskara:
	a = xv**2 + yv**2
	b = 2*(x0*xv + y0*yv)
	c = (x0**2 + y0**2 - R_col**2)
	t_col = (-b - np.sqrt(b**2 - 4*a*c) ) / (2*a)
	if ( (t_col < 0) or (t_col > t_max) ) :
		return ( force, p_col)
	p_col = p0 + t_col*v
	if func_t == None:
		force = F_max * (t_max-t_col)/t_max
	else:
		force = F_max * func_t(t_col)
	force = - force * p_col / np.linalg.norm(p_col)
	return ( force, p_col )
		

def avoid_closest_point(p0, v, F_max=25.0, R_a=0.6, R_o=0.3, R_d=0.3, t_max=5.0,
                        func_t=None,func_d=None):
	R_col = R_a + R_o
	R_desv = R_col + R_d
	force = np.array([0.,0.])
	p_col = None
	if np.linalg.norm(v) == 0.0 :
		return ( force, p_col )
	r_min = p0 - v * np.dot(p0,v) / np.linalg.norm(v)**2
	r_min_norm = np.linalg.norm(r_min)
	if r_min_norm >= R_desv :
		return ( force, p_col )
	elif r_min_norm <= R_col :
		return avoid_collision_point(p0,v, F_max,R_a,R_o,t_max,func_t)
	else :
		t_col = np.dot(r_min-p0,v) / np.linalg.norm(v)**2
		if ( (t_col < 0) or (t_col > t_max) ) :
			return ( force, p_col)
		if func_t == None:
			force = F_max * (t_max-t_col)/t_max
		else:
			force = F_max * func_t(t_max)
		if func_d == None:
			force = force * (R_desv - r_min_norm) / R_d
		else :
			force = force * func_d(r_min_norm)
		force = - force * r_min / np.linalg.norm(r_min)
		return ( force, r_min )
	
def avoid_critical_point(p0, v, F_max=25.0, R_a=0.6, R_o=0.3, R_d=0.3, t_max=5.0,
                         func_t=None,func_d=None):
	R_col = R_a + R_o
	R_desv = R_col + R_d
	force = np.array([0.,0.])
	p_col = None
	if (np.linalg.norm(v) == 0.0) or (np.dot(v,p0) > 0.0) :
		return ( force, p_col )
	r_min = p0 - v * np.dot(p0,v) / np.linalg.norm(v)**2
	r_min_norm = np.linalg.norm(r_min)
	t_min = np.dot(r_min-p0,v) / np.linalg.norm(v)**2
	
	if func_t == None :
		func_t = lambda t : ( ((t_max-t)/t_max) if (t>0 and t<t_max) else 0.0)
	if func_d == None :
		func_d = lambda d : ( ((R_desv-d)/R_d) if (d>R_col and d<R_desv) else
		                       1.0             if (d<=R_col)             else
		                       0.0                                           )
	t_crit = 0.0
	maxval = 0.0
	for i in range(100) :
		t = i*t_max/100
		total = func_t(t) * func_d(np.linalg.norm(p0+t*v))
		if total > maxval :
			t_crit = t
			maxval = total
	r_crit = p0+t_crit*v
	if np.linalg.norm(r_crit) != 0:
		force = - maxval * F_max * r_crit / np.linalg.norm(r_crit)
	else:
		force = - maxval * F_max * np.array([1.0,0.0])
	return (force, r_crit)

