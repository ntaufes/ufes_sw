#!/usr/bin/env python2
# -*- encoding: UTF-8 -*- 

import os
import time

import rosbag
from sensor_msgs.msg import LaserScan

import numpy as np
import matplotlib.pyplot as plt

from ..algs import *

LASER_OFFSET = 0.3

if __name__ == "__main__" :
	bag = rosbag.Bag(os.path.split(__file__)[0] + "/testbag.bag")
	msglist = []
	parsedlist = []
	for topic, msg, t in bag.read_messages(topics = ["/roslaser"]):
		msglist.append(msg)
		msg = LaserScan(header = msg.header,
		                angle_min = msg.angle_min,
		                angle_max = msg.angle_max,
		                angle_increment = msg.angle_increment,
		                time_increment = msg.time_increment,
		                scan_time = msg.scan_time,
		                range_min = msg.range_min,
		                range_max = msg.range_max,
		                ranges = msg.ranges,
		                intensities = msg.intensities,
		               )
		parsedlist.append(msg)
	pass
	fig = plt.figure()
	ax  = plt.axes()
	fig.show()
	plot_points = plt.plot(0,0,'o')
	plot_nearest = plt.plot(0,0,'rx')
	plot_force = ax.quiver(0,0,color='r', scale=50.0)
	plot_force.zorder = 2
	plot_me = plt.Circle((0,0), 0.5, color='g')
	ax.add_artist(plot_me)
	plt.gca().set_xlim((-8.,8.))
	plt.gca().set_ylim((-3.,7.))
	plt.gca().set_autoscale_on(False) # Configuracao removida toda vez que clf ou cla eh chamado
	plt.gca().set_aspect('equal', 'datalim')
	plt.grid()
	for msg in parsedlist :
		points = scan2array(msg)
		points = filter_dist_angle(points,
		                           msg.range_min, np.inf, # msg.range_max
		                           -np.pi/2, np.pi/2    )
		if points.shape[0] == 0 :
			continue
			#points = np.array( [ [np.inf, 0] ])
		x,y = pol2cart(points[:,0:1], points[:,1:2])
		y = y + LASER_OFFSET
		points = np.concatenate( (x,y), axis = 1)
		force,closest = pot_field_4points(points, outer_radius=4.0)
		plt.title('t=' + str(msg.header.stamp.to_sec()) + ' f=' + str(force))
		plot_points[0].set_data(-y,x)
		plot_nearest[0].set_data(-closest[:,1], closest[:,0])
		plot_force.U = -np.array((force[1],))
		plot_force.V = +np.array((force[0],))
		#
		# Versao ninja da internet:  # Versão lerda to tutorial:
		fig.canvas.draw()            # plt.draw()
		fig.canvas.flush_events()    # plt.pause(0.01)
		# fonte: http://bastibe.de/2013-05-30-speeding-up-matplotlib.html
		# Aparentemente, da pra otimizar ainda mais.
		#  A sacada e nao atualizar todo mundo, e sim só quem mudou. Chame
		#  ax.get_children() pra ver uma lista com todo mundo que é
		#  atualizado no flush_events
		#
