#!/usr/bin/env python2
# -*- encoding: UTF-8 -*- 

import os
import time

import rosbag
from geometry_msgs.msg import TwistStamped
from sensor_msgs.msg import LaserScan

import numpy as np
import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from ..algs import *

LASER_OFFSET = 0.3

def openbag(bagname) :
	bag = rosbag.Bag(bagname)
	msglist = []
	parsedlist = []
	odomlist = []
	for topic, msg, t in bag.read_messages(topics = ["/roslaser", "/walker_setpoint"]):
		msglist.append(msg)
		if topic == "/roslaser" :
			msg = LaserScan(header = msg.header,
			                angle_min = msg.angle_min,
			                angle_max = msg.angle_max,
			                angle_increment = msg.angle_increment,
			                time_increment = msg.time_increment,
			                scan_time = msg.scan_time,
			                range_min = msg.range_min,
			                range_max = msg.range_max,
			                ranges = msg.ranges,
			                intensities = msg.intensities,
			               )
			parsedlist.append(msg)
		elif topic == "/walker_setpoint":
			msg = TwistStamped(header=msg.header, twist=msg.twist)
			odomlist.append(msg)
	return parsedlist,odomlist

if __name__ == "__main__" :
	parsedlist,odomlist = openbag(os.path.split(__file__)[0] + "/testbag2.bag")
	fig = plt.figure()
	ax  = plt.axes()
	fig.show()
	plot_points = plt.scatter((0.,), (0.,), c=('r',))
	plot_objs = plt.scatter( (),(),c=('g',), s=1000.0)
	plot_origins = plt.scatter( (),(), c=np.array([0.5,0.5,0.5,1.0]), s=1000.0)
	plot_force = ax.quiver(0,0,color='r', scale=50.0)
	plot_sforce = ax.quiver(0,0,color='b', scale=50.0)
	plot_speeds = ax.quiver( (),(),(),(), color='g', scale=10.0)
	plot_force.zorder = 2
	plot_sforce.zorder = 2
	plot_speeds.zorder = 2
	plot_me = plt.Circle((0,0), 0.5, color='g')
	ax.add_artist(plot_me)
	plt.gca().set_xlim((-8.,8.))
	plt.gca().set_ylim((-3.,7.))
	plt.gca().set_autoscale_on(False) # Configuracao removida toda vez que clf ou cla eh chamado
	plt.gca().set_aspect('equal', 'box')
	plt.grid()
	current_odom = 0
	ODOM_MAX = len(odomlist)
	tracker = DBObjectTracker()
	last_time = odomlist[0].header.stamp.to_sec()
	vel_lin = 0.0
	for msg in parsedlist :
		# ODOMETRIA:
		T = np.eye(3)
		while ( odomlist[current_odom].header.stamp<msg.header.stamp
		        and current_odom<ODOM_MAX-1) :
			current_odom = current_odom + 1
			current_time = odomlist[current_odom].header.stamp.to_sec()
			dt = current_time - last_time
			last_time = current_time
			vel_lin = odomlist[current_odom].twist.linear.x
			vel_ang = odomlist[current_odom].twist.angular.z
			D = dt * vel_lin
			dtheta = dt*vel_ang
			if dtheta != 0.0 :
				R = D/dtheta
				dx = R*np.sin(dtheta)
				dy = R*(1.0 - np.cos(dtheta))
			else:
				dx = D
				dy = 0.0
			c = np.cos(dtheta)
			s = np.sin(dtheta)
			N = np.array( [  [  c , s  , -(c*dx+s*dy) ],
			                 [ -s , c  , -(c*dy-s*dx) ],
			                 [ 0. , 0. ,      1.0     ]
			              ] )
			T = np.matmul(N,T)
		
		points = scan2array(msg)
		points = filter_dist_angle(points,
		                           msg.range_min, np.inf, # msg.range_max
		                           -np.pi/2, np.pi/2    )
		if points.shape[0] == 0 :
			continue
		x,y = pol2cart(points[:,0:1], points[:,1:2])
		y = y + LASER_OFFSET
		points = np.concatenate( (x,y), axis=1)
		
		tracker.update(points, last_time, T)

		mobjs = tracker.get_moving_objs()
		sobjs = tracker.get_static_objs()
		laserpoints = [ np.zeros((0,2)) ]
		laserpoints_c = [ np.zeros((0,4)) ]
		object_centers = [ np.zeros((0,2)) ]
		object_c = [ np.zeros((0,4)) ]
		mov_object_centers = [ np.zeros((0,2)) ]
		mov_object_vectors = [ np.zeros((0,2)) ]
		sta_object_origins = [ np.zeros((0,2)) ]
		for mo in mobjs :
			laserpoints.append(mo[2][:,0:2])
			object_centers.append(mo[1][None,0:2])
			mov_object_centers.append(mo[1][None,0:2])
			mov_object_vectors.append(mo[3][None,0:2])
			pointn = mo[2].shape[0]
			laserpoints_c.append( np.ones((pointn,1)) * np.array([(0.0,1.0,0.0,1.0)]) )
			object_c.append(np.array( [(0.0,1.0,0.0,1.0)] ))
		for so in sobjs :
			laserpoints.append(so[2][:,0:2])
			object_centers.append(so[1][None,0:2])
			sta_object_origins.append(so[4][None,0:2])
			pointn = so[2].shape[0]
			laserpoints_c.append( np.ones((pointn,1)) * np.array([(0.0,0.0,0.0,1.0)]) )
			object_c.append(np.array( [(0.0,0.0,0.0,1.0)] ))
		#OUTLIERS:
		outliers = tracker.outliers
		no = outliers.shape[0]
		laserpoints.append(outliers)
		laserpoints_c.append(np.ones((no,1)) * np.array([[0.5,0.5,0.5,1.0]]))
		laserpoints = np.concatenate(laserpoints, axis=0)
		laserpoints_c = np.concatenate(laserpoints_c, axis=0)
		object_centers = np.concatenate(object_centers, axis=0)
		object_c = np.concatenate(object_c, axis=0)
		mov_object_centers = np.concatenate(mov_object_centers, axis=0)
		mov_object_vectors = np.concatenate(mov_object_vectors, axis=0)
		sta_object_origins = np.concatenate(sta_object_origins, axis=0)
		
		laserpoints = np.concatenate((-laserpoints[:,1:2], laserpoints[:,0:1]), axis=1)
		object_centers = np.concatenate((-object_centers[:,1:2], object_centers[:,0:1]), axis=1)
		mov_object_centers = np.concatenate((-mov_object_centers[:,1:2], mov_object_centers[:,0:1]), axis=1)
		mov_object_vectors = np.concatenate((-mov_object_vectors[:,1:2], mov_object_vectors[:,0:1]), axis=1)
		sta_object_origins = np.concatenate((-sta_object_origins[:,1:2], sta_object_origins[:,0:1]), axis=1)
		
		plot_points.set_offsets(laserpoints)
		plot_points.set_facecolor(laserpoints_c)

		plot_objs.set_offsets(object_centers)
		plot_objs.set_facecolor(object_c)

		plot_origins.set_offsets(sta_object_origins)
		
		plot_speeds.set_offsets(mov_object_centers)
		plot_speeds.U = mov_object_vectors[:,0]
		plot_speeds.V = mov_object_vectors[:,1]

		(force, sforce) = social_nav(tracker, np.array([ vel_lin, 0.0]))
		

		#colors = [plt.cm.Accent(each) for each in np.linspace(0,1,10)]
		#trackedpoints = [each.points for each in tracker.objects]
		#trackedpoints = np.concatenate(trackedpoints, axis=0)
		#plot_points.set_offsets(np.concatenate((-trackedpoints[:,1:2],trackedpoints[:,0:1]), axis=1))
		#labels = [ (each.id*np.ones(each.points.shape[0], dtype=np.int)) for each in tracker.objects]
		#labels = np.concatenate(labels, axis=0)
		#plot_points.set_facecolor( np.array([ colors[each%6] for each in labels]) )
		
		plt.title('t=' + str(msg.header.stamp.to_sec()) + ' f=' + str(force))
		plot_force.U = -np.array((force[1],))
		plot_force.V = +np.array((force[0],))
		
		plot_sforce.U = -np.array((sforce[1],))
		plot_sforce.V = +np.array((sforce[0],))
		#
		# Versao ninja da internet:  # Versão lerda to tutorial:
		fig.canvas.draw()            # plt.draw()
		fig.canvas.flush_events()    # plt.pause(0.01)
		# fonte: http://bastibe.de/2013-05-30-speeding-up-matplotlib.html
		# Aparentemente, da pra otimizar ainda mais.
		#  A sacada e nao atualizar todo mundo, e sim só quem mudou. Chame
		#  ax.get_children() pra ver uma lista com todo mundo que é
		#  atualizado no flush_events
		#
