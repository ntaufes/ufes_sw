#!/usr/bin/env python2

import exceptions

import rospy
import tf
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float64, String, UInt8, Int32, ColorRGBA, Bool, Duration, Header
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import Pose, Vector3, Point, Quaternion

from navwalker_algs import algs, rviz_helper

import numpy as np

LASER_OFFSET = 0.4
OUTER_RADIUS = 3.0
INNER_RADIUS = 0.5
MAX_FORCE = 25.0

def callback(laser):
	global pubx, puby, pubmarker
	polar = algs.scan2array(laser)
	polar = algs.filter_dist_angle(
	               polar, laser.range_min, np.inf,
	               -np.pi/2, np.pi/2
	               )
	x,y = algs.pol2cart(polar[:,0], polar[:,1])
	y += LASER_OFFSET
	x,y = (x[:,None], y[:,None])
	xy = np.concatenate((x,y), axis=1)
	(force, closest) = algs.avoid_closest(
	                          xy, max_force=MAX_FORCE,
	                          inner_radius=INNER_RADIUS,
	                          outer_radius=OUTER_RADIUS
	                          )
	forcex = float(force[0])
	forcey = float(force[1])
	pubx.publish(Float64(forcex))
	puby.publish(Float64(forcey))
	rviz_helper.show_force_vector(pubmarker, "base_link",
                                      np.sqrt(forcex**2 + forcey**2),
	                              np.arctan2(forcey,forcex)     )

def main_loop() :
	global pubx, puby, pubmarker, LASER_OFFSET, OUTER_RADIUS
        global INNER_RADIUS, MAX_FORCE
	rospy.init_node("avoid_closest")
	rospy.Subscriber("laser", LaserScan, callback)
	pubx = rospy.Publisher("force_x", Float64, queue_size = 10)
	puby = rospy.Publisher("force_y", Float64, queue_size = 10)
	pubmarker = rospy.Publisher("navmarkers", MarkerArray, queue_size = 10)
	LASER_OFFSET = rospy.get_param('laser_offset', LASER_OFFSET)
	OUTER_RADIUS = rospy.get_param('~outer_radius', OUTER_RADIUS)
	INNER_RADIUS = rospy.get_param('~inner_radius', INNER_RADIUS)
	MAX_FORCE = rospy.get_param('~max_force', MAX_FORCE)
	rospy.spin()

if __name__ == "__main__" :
	try:
		main_loop()
	except rospy.ROSInterruptException :
		pass

