import math
import exceptions

def kinetic_timestep(m,F,v,dt,B,Fmin):
	if (B < 0.0):
		raise exceptions.ValueError('B must be nonnegative')
	if (Fmin < 0.0):
		raise exceptions.ValueError('Fmin must be nonnegative')
	if (m <= 0):
		raise exceptions.ValueError('mass must be positive')
	while dt > 0:
		sign = math.copysign(1.0,v)
		if v == 0.0 :
			sign = math.copysign(1.0,F)
		# signal changes are undone at the end of the calculation
		v = sign*v
		F = sign*F
		if B == 0.0: # coulomb friction
			if v==0 :
				if F > Fmin: # gaining speed from 0
					v = dt*(F-Fmin)/m
					dt = 0.0
				else: # cannot beat static friction
					v = 0.0
					dt = 0.0
			elif v>0:
				dv = (F-Fmin)/m
				if dv >= 0.0 : #speed not decreasing
					v = v + dt*dv
					dt = 0.0
				else : # velocity decreasing
					time_to_zero = -v/dv
					if time_to_zero >= dt : # will not reach 0 in this timestep
						v = v + dt*dv
						dt = 0.0
					else : # zero crossing detected
						v = 0.0
						dt = dt - time_to_zero
		elif B>0.0 and Fmin==0.0 : # linear drag, solution is exponential decay
			k = math.exp(-B*dt/m)
			v = k*v + (1-k)*(F/B)
			dt = 0
		elif B>0.0 and Fmin>0.0 : # linear/coulomb drag (greater wins)
			v_lim = Fmin/B # at this speed drag switches from linear to constant
			if v>v_lim or ( v==v_lim and (F-Fmin)>=0.0 ): #linear drag region
				v_assympt = F/B #assymptotic velocity
				if v_assympt >= v_lim :
					k = math.exp(-B*dt/m)
					v = k*v + (1-k)*v_assympt
					dt = 0.0
				else : # velocity decreasing
					time_to_coulomb = (m/B) * math.log( (v-v_assympt) / (v_lim-v_assympt) )
					if time_to_coulomb < dt: # will reach coulomb drag region
						v = v_lim
						dt = dt - time_to_coulomb
					else: # won't reach coulomb drag region
						k = math.exp(-B*dt/m)
						v = k*v + (1-k)*v_assympt
						dt = 0.0
			elif v<v_lim or ( v==v_lim and (F-Fmin)<0.0 ): # coulomb drag region
				if v==0 and F<=Fmin: # cannot beat static friction
					v = 0.0
					dt = 0.0
				elif v==0 and F>Fmin:
					dv = (F-Fmin)/m
					time_to_linear = v_lim/dv
					if time_to_linear<dt: # will reach linear drag region
						v = v_lim
						dt = dt - time_to_linear
					else: # won't reach linear drag region
						v = dt*dv
						dt = 0.0
				elif v>0:
					dv = (F-Fmin)/m
					if dv>=0:
						time_to_linear = (v_lim-v)/dv
						if time_to_linear<dt: # will reach linear drag region
							v = v_lim
							dt = dt - time_to_linear
						else: # won't reach linear drag region
							v = v + dt*dv
							dt = 0.0
					elif dv<0:
						time_to_zero = -v/dv
						if time_to_zero >= dt : # will not reach 0 in this timestep
							v = v + dt*dv
							dt = 0.0
						else : # zero crossing detected
							v = 0.0
							dt = dt - time_to_zero
		else:
			raise exceptions.ValueError('Erro: isso nao deveria acontecer.')
		v = sign*v
		F = sign*F
	return v


