#!/usr/bin/env python2

import rospy
from std_msgs.msg import Header
from geometry_msgs.msg import Wrench, WrenchStamped

import exceptions
from threading import Lock

listmutex = Lock()
forcelist = []

def sumwrenches(f1, f2):
	ret = Wrench()
	ret.force.x = f1.force.x + f2.force.x
	ret.force.y = f1.force.y + f2.force.y
	ret.force.z = f1.force.z + f2.force.z
	ret.torque.x = f1.torque.x + f2.torque.x
	ret.torque.y = f1.torque.y + f2.torque.y
	ret.torque.z = f1.torque.z + f2.torque.z
	return ret

def update_force_cb(index, force):
	with listmutex :
		forcelist[index] = force	

if __name__ == "__main__":
	try:
		rospy.init_node("add_forces")
		topics = rospy.get_param('~topics')
		rate = rospy.get_param('~rate')
		for i in range(len(topics)) :
			forcelist.append(Wrench())
			rospy.Subscriber(topics[i], Wrench, 
			                 lambda force, index=i: update_force_cb(index, force)
			                )
		pub = rospy.Publisher("sum_wrenches", WrenchStamped, queue_size = 10)
		rate = rospy.Rate(rate)
		while not rospy.is_shutdown() :
			forcesum = WrenchStamped()
			with listmutex :
				forcesum.wrench = forcelist[0]
				for i in range(1, len(forcelist)) :
					
					forcesum.wrench = sumwrenches(forcesum.wrench, forcelist[i])
			forcesum.header = Header()
			forcesum.header.stamp = rospy.Time.now()
			pub.publish(forcesum)
			rate.sleep()

	except rospy.ROSInterruptException :
		pass
	except exceptions.KeyError :
		print("Insert rate and topic names as private parameters")
		pass
