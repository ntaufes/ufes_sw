#!/usr/bin/env python2

import rospy
from std_msgs.msg import Header
from geometry_msgs.msg import Twist, Wrench, TwistStamped, WrenchStamped
from std_srvs.srv import EmptyResponse, Empty

from solver_adm_ctl.analytic_integration import kinetic_timestep

import sys
import exceptions
from threading import Lock

param_lock = Lock()

def get_values(wrench):
	F = wrench.wrench.force.x
	t = wrench.wrench.torque.z
	time = wrench.header.stamp
	return (F, t, time)

def make_vel_msg(v,w,time):
	ret = TwistStamped()
	ret.header = Header()
	ret.header.stamp = time
	ret.twist = Twist()
	ret.twist.linear.x = v
	ret.twist.angular.z = w
	return ret

def callback(wrenchmsg):
	global last_time, v, w, veltopic
	(F,t,time) = get_values(wrenchmsg)
	try:
		dt = (time - last_time).to_sec()
		last_time = time
	except exceptions.NameError :
		dt = 0.0
		last_time = time
	with param_lock:
		#v = kinetic_timestep(m,                 Fr+Fl, v, dt,  B, F_drag_min)
		#w = kinetic_timestep(J, dist_handle*(Fr-Fl)/2, w, dt, Bw, T_drag_min) 
		v = kinetic_timestep(m, F, v, dt, B , F_drag_min)
		w = kinetic_timestep(J, t, w, dt, Bw, T_drag_min)
	velmsg = make_vel_msg(v,w,time)
	veltopic.publish(velmsg)

def update_parameters():
#	Kinetic parameters:
#		m: mass
#		J: moment of inertia
#		B: drag constant [ Newtons / (m/s) ]
#		Bw: angular drag constant [ Newton-meters / (rad/s) ]
#               F_drag_min, T_drag_min: minimal drag force and torque;
	global m, J, B, Bw, F_drag_min, T_drag_min
	names    = (  'm',  'J', 'B', 'Bw', 'F_drag_min', 'T_drag_min')
	defaults = ( 10.0, 1.00, 0.5, 1.0 ,          0.0,          0.0)
	is_defined = tuple(rospy.has_param('~'+x) for x in names)
	for i in range(len(names)) :
		if not is_defined[i] :
			rospy.set_param('~'+names[i], defaults[i])
			rospy.logwarn("Parametro " + names[i] + " nao"   +
			              " definido, usando o valor padrao" +
			              ". (" + str(defaults[i]) + ")"     )
	m = rospy.get_param("~m")
	J = rospy.get_param("~J")
	B = rospy.get_param("~B")
	Bw = rospy.get_param("~Bw")
	F_drag_min = rospy.get_param("~F_drag_min")
	T_drag_min = rospy.get_param("~T_drag_min")

def update_service_cb(req):
	with param_lock:
		update_parameters()
	return EmptyResponse()

def listener():
	global v, w, veltopic
	global m, J, B, Bw
	v = 0.0 # linear velocity
	w = 0.0 # angular velocity
	rospy.init_node('adm_ctl')
	rospy.loginfo("script path:" + str(sys.path)) # TODO: checar path dos imports
	update_parameters()
	rospy.Service('refresh_params', Empty, update_service_cb)
	veltopic = rospy.Publisher('vel_out', TwistStamped, queue_size=10)
	rospy.Subscriber('wrench_in', WrenchStamped, callback)
	rospy.spin()

if __name__ == '__main__':
	try:
		listener()
	except rospy.ROSInterruptException :
		pass
